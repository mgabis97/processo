<?php

<html>
<header>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</header>

<body>

<form class="form-horizontal" action="updateform.php" method="POST">
<fieldset>


<!-- Insere nome-->
<div class="form-group">
  <label class="col-md-4 control-label" for="nome">Nome</label>
  <div class="col-md-4">
  <input id="nome" name="nome" class="form-control input-md" required="" pattern="[a-zA-Z\s]+" type="text">

  </div>
</div>

<!-- Insere email-->
<div class="form-group">
  <label class="col-md-4 control-label" for="email">E-mail</label>
  <div class="col-md-4">
  <input id="email" name="email" class="form-control input-md" required="" type="email">

  </div>
</div>

<!-- insere a Senha-->
<div class="form-group">
  <label class="col-md-4 control-label" for="senha">Senha</label>
  <div class="col-md-4">
    <input id="senha" name="senha" class="form-control input-md" required="" pattern="(?=.*\d)(?=.*[a-zA-Z]).{6,}" type="password">

  </div>
</div>

<!-- botoes-->
<div class="form-group">
  <label class="col-md-4 control-label" for="envia"></label>
  <div class="col-md-8">
    <button id="envia" name="envia" class="btn btn-success">Enviar</button>
    <button type="reset" id="limpa" name="limpa" class="btn btn-inverse">Limpar</button>
  </div>
</div>

</fieldset>
</form>
</body>
</html>



 ?>
