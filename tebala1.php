<?php
require_once 'conecta.php';
$consulta = $conn->query("SELECT * from tabela1");
?>
<header>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</header>

<div class="table-responsive">
    <table class="table table-bordered table-striped table-hover table-sm">
        <tbody>
        <thead>
            <tr>
              <th scope="col">ID</th>
              <th scope="col">Nome</th>
              <th scope="col">Email</th>
              <th scope="col">Senha</th>
              <th scope="col">Ultima Alteracao</th>
            </tr>
          </thead>
          <tbody>
            <?php
        while($row = $consulta->fetch(PDO::FETCH_OBJ)){
          echo "<tr>";
          echo "<th scope='row'>".$row->id . "</th>";
          echo "<td>".$row->nome . "</td>";
          echo "<td>".$row->email . "</td>";
          echo "<td>".$row->senha . "</td>";
          echo "<td>".$row->ultima_alteracao . "</td>";
          echo "</tr>";
        }
        ?>


        </tbody>
    </table>
</div>
